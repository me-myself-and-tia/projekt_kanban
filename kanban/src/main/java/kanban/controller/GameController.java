package kanban.controller;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.context.RequestContext;
import kanban.services.Timer;
import kanban.model.Block;
import kanban.model.Board;
import kanban.model.CurrentGame;
import kanban.model.Level;
import kanban.services.BlockDeleter;
import kanban.services.BoardSetter;
import kanban.services.GameStatus;
import kanban.services.MoveBlocks;

@ManagedBean
@SessionScoped
public class GameController {

	public static final String FORCERELOAD = "?faces-redirect=true";
	
	private CurrentGame game;
	private Timer timer;
	private BoardSetter boardSetter;

	@PostConstruct
	public void init() {
		game = new CurrentGame();
	}
	
	public String startGame() {
		game = new CurrentGame();
		boardSetter = new BoardSetter();
		startLevel();
		this.timer = new Timer(game);
		return "game" + FORCERELOAD;
	}
	
	public void makeMoveInGame(Block block) {
		if (block.isClickable()) {
			int deletedBlocks = deleteBlocks(block);
			moveBlocks();
			updateStatistics(deletedBlocks);
			if(!boardSetter.clickableBlockExists()) {
				if(game.isWon()) {
					RequestContext.getCurrentInstance().execute("gameWon.show()");
				} else {
					RequestContext.getCurrentInstance().execute("gameLost.show()");
				}
			}
		}
	}
	
	private int deleteBlocks(Block block){
		BlockDeleter deleter = new BlockDeleter(block);
		return deleter.deleteBlocksWithSameColour(block);
	}

	private void moveBlocks() {
		MoveBlocks mover = new MoveBlocks(game.getBoard().getBlockList());
		mover.moveBlocks();
		mover.moveColums();
	}
	
	private void updateStatistics(int deletedBlocks) {
		GameStatus status = new GameStatus(game);
		status.updateNumberOfBlocks(deletedBlocks);
		status.updatePoints(deletedBlocks);
	}
	
	public void startLevel() {
		game.setCurrentNumberOfBlocks(Board.HEIGHT * Board.WIDTH);
		game.setCurrentPoints(0);
		game.setCurrentTime(0);
		game.setBoard(boardSetter.initBoard(game.getCurrentLevel()));
	}
	
	private void updateLevel() {
		int levelnumber = game.getCurrentLevel().getLevelNumberAsInt()+1;
		game.setCurrentLevel(Level.valueOf("LEVEL" + levelnumber));
	}
	
	

	
	//Navigation for Pages
	
	public String startNewLevel() {
		updateLevel();
		startLevel();
		return "game"+ FORCERELOAD;
	}


	public String toIndex(){
		return "intro" + FORCERELOAD;
	}
	
	public String toResult(){
		game.setTotalPoints(game.getCurrentPoints()+game.getTotalPoints());
		return "result" + FORCERELOAD;
	}
	
	public String toSaveGamePage(){
		return "savepage" + FORCERELOAD;
	}
	
	public String toLevelFinished(){
		game.setTotalPoints(game.getTotalPoints()+game.getCurrentPoints());
		if(game.getCurrentLevel().isLastlevel()) {
			return "result" + FORCERELOAD;
		}
		return "levelFinished" + FORCERELOAD;
	}
	
	public String toRanking(){
		return "ranking" + FORCERELOAD;
	}

	
	//Getter and Setter
	
	public Timer getTimer() {
		return timer;
	}
	
	public CurrentGame getGame() {
		return game;
	}

	public BoardSetter getBoardSetter() {
		return boardSetter;
	}
}
