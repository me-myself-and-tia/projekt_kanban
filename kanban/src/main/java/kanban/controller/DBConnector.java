package kanban.controller;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import kanban.entities.SavedGame;
import kanban.model.CurrentGame;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

@ManagedBean
@SessionScoped
public class DBConnector implements Serializable{
	private static final long serialVersionUID = 1083807531575480822L;
	private static final SessionFactory sessionFactory = buildSessionFactory();
	
	private Session session;
	private SavedGame saveGame;

	private static SessionFactory buildSessionFactory() {
        try {
            return new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }
	
	@PostConstruct
    public void init() {
        SessionFactory factory = getSessionFactory();
        session = factory.openSession();
    }
    

	@SuppressWarnings("unchecked")
	public List<SavedGame> getAllSavedGames() {
        session.beginTransaction();
        String queryString = "FROM SavedGame ORDER BY points DESC";
        Query sqlQuery = session.createQuery(queryString);
    	List<SavedGame> savedGames = sqlQuery.list();
        session.getTransaction().commit();
        return addRanks(savedGames);
	}
	
	private List<SavedGame> addRanks(List<SavedGame> savedGames) {
		int rank = 0;
		int points = 0;
		
		for (SavedGame savedGame : savedGames) {
			if(savedGame.getTotalPoints() != points) {
				rank++;
				savedGame.setRank(rank);
			} else {
				savedGame.setRank(rank);
			}
			points = savedGame.getTotalPoints();
		}
		return savedGames;
	}
	
	public String toSaveGame(CurrentGame currentGame) {
		saveGame = new SavedGame();
		HttpServletRequest request = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String userName = request.getParameter("name-input:username");
        saveGame.setUserName(userName);
		saveGame.setDate(new Date(System.currentTimeMillis()));
		saveGame.setTotalPoints(currentGame.getTotalPoints());
		saveGame.setLevelNumber(currentGame.getCurrentLevel().getLevelNumberAsInt());
		session.beginTransaction();
		session.save(saveGame);
		session.getTransaction().commit();
		return "ranking"+ GameController.FORCERELOAD;
	}
	
	// Getter and Setter
	 
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

	public SavedGame getSaveGame() {
		return saveGame;
	}
}
