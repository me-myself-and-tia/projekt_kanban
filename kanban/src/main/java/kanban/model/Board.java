package kanban.model;


import java.util.List;

public class Board {

	public static final int WIDTH =  40;
	public static final int HEIGHT = 20;
	private List<Block> blockList;

	public Board(List<Block> blockList) {
		this.blockList = blockList;
	}
	
	
	public int getWidth() {
		return WIDTH;
	}

	public int getHeight() {
		return HEIGHT;
	}

	public List<Block> getBlockList() {
		return blockList;
	}
	
}
