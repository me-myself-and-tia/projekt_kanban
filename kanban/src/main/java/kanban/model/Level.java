package kanban.model;

public enum Level {
	
	LEVEL1(3,30),
	LEVEL2(3,20),
	LEVEL3(4,30),
	LEVEL4(4,20);
	
	private int numberOfColours;
	private int goal;
	
	private Level(int numberOfColours, int leftOverBlocks) {
		this.numberOfColours = numberOfColours;
		this.goal = leftOverBlocks;
	}

	public int getNumberOfColours() {
		return numberOfColours;
	}

	public int getGoal() {
		return goal;
	}

	public String getLevelNumber() {
		return this.name().substring(5); 
	}
	
	public int getLevelNumberAsInt() {
		return Integer.parseInt(getLevelNumber());
	}
	
	public boolean isLastlevel() {
		if(this == LEVEL4) {
			return true;
		}
		return false;
	}
}
