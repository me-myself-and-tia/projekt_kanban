package kanban.model;

import java.util.Random;

public enum Colour {
	AQUA(),
	FUCHSIA(),
	GREEN(),
	LIME(),
	NAVY(),
	ORANGE(),
	PURPLE(),
	RED(),
	TEAL(),
	YELLOW(),
	TRANSPARENT();
	
	private static final Colour[] VALUES = values();
	private static final int LENGTH = VALUES.length;
	private static Random randomizer = new Random();

	public static Colour getRandomColour() {
		return VALUES[randomizer.nextInt(LENGTH)];
	}
	
	public String toString() {
		return name().toLowerCase();
	}
}
