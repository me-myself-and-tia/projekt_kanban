package kanban.model;


public class CurrentGame {

	private int totalPoints;
	private int currentPoints;
	private int currentNumberOfBlocks;
	private Board board;

	private Level currentLevel;
	private int currentTime;

	public CurrentGame() {
		this.currentLevel = Level.LEVEL1;
		this.totalPoints = 0;
	}
	
	public boolean isWon() {
		if(this.currentNumberOfBlocks <= this.currentLevel.getGoal()){
			return true;
		}
		return false;
	}

	
	// Getter and Setter
	
	public int getCurrentTime() {
		return currentTime;
	}

	public void setCurrentTime(int currentTime) {
		this.currentTime = currentTime;
	}
	
	public int getCurrentPoints() {
		return currentPoints;
	}
	
	public void setCurrentPoints(int currentPoints) {
		this.currentPoints = currentPoints;
	}
	
	public int getCurrentNumberOfBlocks() {
		return currentNumberOfBlocks;
	}
	
	public void setCurrentNumberOfBlocks(int currentNumberOfBlocks) {
		this.currentNumberOfBlocks = currentNumberOfBlocks;
	}
	
	public Level getCurrentLevel() {
		return currentLevel;
	}
	
	public void setCurrentLevel(Level currentLevel) {
		this.currentLevel = currentLevel;
	}

	public int getTotalPoints() {
		return totalPoints;
	}

	public Board getBoard() {
		return board;
	}
	
	public void setBoard(Board board) {
		this.board = board;
	}

	public void setTotalPoints(int totalPoints) {
		this.totalPoints = totalPoints;
	}
}
