package kanban.model;

import java.util.ArrayList;
import java.util.List;

public class Block {

	public static final int UP = 0;
	public static final int RIGHT = 1;
	public static final int DOWN = 2;
	public static final int LEFT = 3;
	private static final int NUMBEROFNEIGHBOURS = 4;
	 
	private Colour colour;
	private List<Block> neighbours;
	
	public Block(Colour colour) {
		this.colour = colour;
		neighbours= new ArrayList<Block>();
		for (int i = 0; i< NUMBEROFNEIGHBOURS; i++){
			neighbours.add(i, null);
		}
	}
	
	public boolean isClickable(){
		if(colour == Colour.TRANSPARENT) {
			return false;
		}
		for (Block neighbour : getNeighbours()) {
			if(neighbour != null && colour == neighbour.getColour()){
				return true;
			}
		}
		return false;
	}
	
	public boolean isTransparent() {
		if(colour == Colour.TRANSPARENT) {
			return true;
		}
		return false;
	}
	

	
	// Getter and Setter
	
	
	public Colour getColour() {
		return colour;
	}

	public void setColour(Colour colour) {
		this.colour = colour;
	}

	public List<Block> getNeighbours() {
		return neighbours;
	}
}
