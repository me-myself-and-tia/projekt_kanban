package kanban.services;

import kanban.model.CurrentGame;

public class Timer {

	private CurrentGame game;
	private boolean running;
	
	public Timer(CurrentGame game) {
		this.game = game;
		this.running = true;
	}
	
	public String timeToMinutes() {
		String minutes = String.valueOf((int)Math.floor(game.getCurrentTime()/60));
		String seconds = String.valueOf(game.getCurrentTime() % 60);
		if(minutes.length() < 2) {
			minutes = "0" + minutes;
		}
		if(seconds.length() < 2) {
			seconds = "0" + seconds;
		}
		return minutes + ":" + seconds;
	}
	
	public void incrementTime() {
		int oldTime = game.getCurrentTime();
		game.setCurrentTime(oldTime+1);
	}
	

	public boolean isRunning() {
		return this.running;
	}
	
	public void updateRunning(boolean running) {
		this.running = running;
	}
}
