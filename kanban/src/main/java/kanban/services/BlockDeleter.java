package kanban.services;

import kanban.model.Block;
import kanban.model.Colour;


public class BlockDeleter {
	
	private Colour colour;
	
	public BlockDeleter(Block block) {
		this.colour = block.getColour();
		
	}
	
	public int deleteBlocksWithSameColour(Block block) { 
		int deletedBlocks = 1;
		block.setColour(Colour.TRANSPARENT);
		for (Block neighbour : block.getNeighbours()) {
			if(neighbour != null &&	colour == neighbour.getColour()) {
				deletedBlocks += deleteBlocksWithSameColour(neighbour);
			}
		}
		return deletedBlocks;
	}
}
