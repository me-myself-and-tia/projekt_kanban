package kanban.services;

import java.util.ArrayList;
import java.util.List;

import kanban.model.Block;
import kanban.model.Board;
import kanban.model.Level;

public class BoardSetter {

	private ColourRandomizer colRandomizer;
	private List<Block> blockList;
	
	public BoardSetter() {
	}
	
	public Board initBoard(Level level){
		colRandomizer = new ColourRandomizer();
		colRandomizer.createPoolOfColours(level);
		return new Board(fillBlockArrayList());
	}
	
	
	private List<Block> fillBlockArrayList() {
		blockList = new ArrayList<Block>();
		while(blockList.size() < (Board.WIDTH * Board.HEIGHT)) {
			blockList.add(new Block(colRandomizer.randomizeColour()));
		}
		NeighbourSetter neighbour = new NeighbourSetter(blockList); 
		for (Block block : blockList) {
			neighbour.updateNeighbours(block);
		}
		if(clickableBlockExists()) {
			return blockList;
		}	
		return fillBlockArrayList();
	}
	
	public boolean clickableBlockExists() {
		for (Block block : blockList) {
			if(block.isClickable()) {
				return true;
			}
		}
		return false;
	}
	
	// Getter and setter
	
	public ColourRandomizer getColRandomizer() {
		return colRandomizer;
	}
}
