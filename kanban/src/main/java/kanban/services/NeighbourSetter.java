package kanban.services;

import java.util.List;

import kanban.model.Block;
import kanban.model.Board;

public class NeighbourSetter {
	
	private List<Block> allBlocks;
	

	public NeighbourSetter(List<Block> blockList){
		this.allBlocks = blockList;
	}
	
	public void updateNeighbours(Block block){
		int index= allBlocks.indexOf(block) ;
		List<Block> neighbours = block.getNeighbours();
		
		if (index >= Board.WIDTH){
			neighbours.set(Block.UP, allBlocks.get(index-Board.WIDTH));
		}
		if (index % Board.WIDTH != Board.WIDTH-1){
			neighbours.set(Block.RIGHT, allBlocks.get(index+1));
		}
		if (index < allBlocks.size()-Board.WIDTH){
			neighbours.set(Block.DOWN, allBlocks.get(index+Board.WIDTH));
		}
		if (index % Board.WIDTH != 0){
			neighbours.set(Block.LEFT, allBlocks.get(index-1));
		}
	}
}
