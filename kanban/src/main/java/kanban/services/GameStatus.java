package kanban.services;

import kanban.model.CurrentGame;

public class GameStatus {

	private CurrentGame game;
	private static final int POINTSPERBLOCK = 5;
	
	public GameStatus(CurrentGame game){
		this.game = game;
	}

	public void updateNumberOfBlocks(int numberOfDeletedBlocks){
		game.setCurrentNumberOfBlocks(game.getCurrentNumberOfBlocks()- numberOfDeletedBlocks);
	}
	
	public void updatePoints(int numberOFDeletedBlocks) {
		game.setCurrentPoints(game.getCurrentPoints() + numberOFDeletedBlocks*POINTSPERBLOCK);
	}
}
