package kanban.services;

import java.util.List;

import kanban.model.Block;
import kanban.model.Board;
import kanban.model.Colour;

public class MoveBlocks {
	
	private List<Block> blocks;
	
	public MoveBlocks(List<Block> blocks) {
		this.blocks = blocks;
	}
	
	public void moveBlocks(){
		Block above, current;
		for (int index = blocks.size()-1; index >= Board.WIDTH; index--) {
			current = blocks.get(index);
			if(current.isTransparent()) {
				above = current.getNeighbours().get(Block.UP);
				updateColourOfBlock(above, index);
			}
		}
	}

	private void updateColourOfBlock(Block above, int index) {
		int counter = 2;
		while (true){
			int indexOfBlockTwoRowsUp = index-counter* Board.WIDTH;
			if(!above.isTransparent()) {
				blocks.get(index).setColour(above.getColour());
				above.setColour(Colour.TRANSPARENT);
			} else if (above.isTransparent() && indexOfBlockTwoRowsUp >= 0) {
				above = blocks.get(indexOfBlockTwoRowsUp);
					counter++;
					continue;
			}
			break;
		}
	}

	
	public void moveColums(){
		for(int i = blocks.size()-1; i >= blocks.size()-Board.WIDTH; i--) {
			if(blocks.get(i).getColour() == Colour.TRANSPARENT) {
				moveAllColumnsFartherRight(i);
			}
		}
	}


	private void moveAllColumnsFartherRight(int i) {
		for (Block block : blocks) {
			if(blocks.indexOf(block) % Board.WIDTH > i % Board.WIDTH) {
				block.getNeighbours().get(Block.LEFT).setColour(block.getColour());
				block.setColour(Colour.TRANSPARENT);
			}
		}
	}
	
}
