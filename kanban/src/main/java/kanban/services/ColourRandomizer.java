package kanban.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import kanban.model.Colour;
import kanban.model.Level;

public class ColourRandomizer {
	
	private static final int FIRSTCOLOURINLIST = 0;
	
	private List<Colour> poolOfColours;

	public void createPoolOfColours(Level level){
		Colour randomColour;
		poolOfColours = new ArrayList<Colour>();
		while(poolOfColours.size() < level.getNumberOfColours()) {
			randomColour = Colour.getRandomColour();
			if(randomColour != Colour.TRANSPARENT && !poolOfColours.contains(randomColour)) {
				poolOfColours.add(randomColour);
			}
		}
	}
	
	public Colour randomizeColour() {
		Collections.shuffle(poolOfColours);
		return poolOfColours.get(FIRSTCOLOURINLIST);
	}
	
	
	// Getter and Setter
	
	public List<Colour> getPoolOfColours() {
		return poolOfColours;
	}
}
