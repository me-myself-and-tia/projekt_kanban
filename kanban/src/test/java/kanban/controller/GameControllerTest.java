package kanban.controller;

import static org.junit.Assert.*;
import kanban.model.Block;
import kanban.model.Colour;
import kanban.model.CurrentGame;
import kanban.model.Level;
import kanban.services.NeighbourSetter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GameControllerTest {

	private GameController controller;
	private CurrentGame game;
	
	@Before
	public void setUp() throws Exception {
		controller = new GameController();
		controller.init();
	}

	@After
	public void tearDown() throws Exception {
		controller = null;
	}
	
	@Test
	public void startGameOverwritesGameFromInitMethod() {
		game = controller.getGame();
		controller.startGame();
		assertNotSame(game, controller.getGame());
	}
	
	@Test
	public void startGameSetsCurrentNumberOfBlocksCorrectly() {
		controller.startGame();
		assertEquals(800, controller.getGame().getBoard().getBlockList().size());
	}
	
	@Test
	public void makeMoveDoesNotUpdatePointsIfBlockIsNotClickable() {
		Block block = new Block(Colour.TRANSPARENT);
		controller.getGame().setCurrentPoints(100);
		controller.makeMoveInGame(block);
		assertSame(100, controller.getGame().getCurrentPoints());
	}
	
	@Test
	public void makeMoveDoesNotUpdateBlocksIfBlockIsNotClickable() {
		Block block = new Block(Colour.TRANSPARENT);
		controller.getGame().setCurrentNumberOfBlocks(200);
		controller.makeMoveInGame(block);
		assertEquals(200, controller.getGame().getCurrentNumberOfBlocks());
	}
	
	@Test
	public void toLevelFinishedAddsToTotalPoints() {
		controller.getGame().setCurrentPoints(234);
		controller.getGame().setTotalPoints(432);
		controller.toLevelFinished();
		assertEquals(666, controller.getGame().getTotalPoints());
	}
	
	@Test
	public void startNewLevelSetsAllValuesAndUpdatesLevel() {
		controller.startGame();
		controller.getGame().setCurrentLevel(Level.LEVEL2);
		controller.startNewLevel();
		assertEquals(Level.LEVEL3, controller.getGame().getCurrentLevel());
		assertEquals(800, controller.getGame().getCurrentNumberOfBlocks());
		assertEquals(0, controller.getGame().getCurrentPoints());
		assertEquals(0, controller.getGame().getCurrentTime());
		assertNotNull(controller.getGame().getBoard());
	}
	
	@Test
	public void makeMoveInGameUpdatesStatistics() {
		controller.startGame();
		Block currentBlock = new Block(Colour.NAVY);
		controller.getGame().getBoard().getBlockList().set(100, currentBlock);
		prepareNeighbouringBlocks();		
		NeighbourSetter neighbourSetter = new NeighbourSetter(controller.getGame().getBoard().getBlockList());
		for (Block block : controller.getGame().getBoard().getBlockList()) {
			neighbourSetter.updateNeighbours(block);
		}

		controller.makeMoveInGame(currentBlock);
		
		assertEquals(797, controller.getGame().getCurrentNumberOfBlocks());
		assertEquals(15, controller.getGame().getCurrentPoints());
	}

	// Helper methods
	
	private void prepareNeighbouringBlocks() {
		controller.getGame().getBoard().getBlockList().set(99, new Block(Colour.GREEN));

		controller.getGame().getBoard().getBlockList().set(101, new Block(Colour.NAVY));
		controller.getGame().getBoard().getBlockList().set(60, new Block(Colour.GREEN));
		controller.getGame().getBoard().getBlockList().set(140, new Block(Colour.YELLOW));
		
		controller.getGame().getBoard().getBlockList().set(102, new Block(Colour.NAVY));
		controller.getGame().getBoard().getBlockList().set(61, new Block(Colour.YELLOW));
		controller.getGame().getBoard().getBlockList().set(141, new Block(Colour.RED));
		
		controller.getGame().getBoard().getBlockList().set(103, new Block(Colour.YELLOW));
		controller.getGame().getBoard().getBlockList().set(62, new Block(Colour.RED));
		controller.getGame().getBoard().getBlockList().set(142, new Block(Colour.YELLOW));
	}
}
