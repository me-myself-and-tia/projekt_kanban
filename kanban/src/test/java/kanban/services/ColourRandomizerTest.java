package kanban.services;

import static org.junit.Assert.*;

import kanban.model.Level;
import kanban.model.Colour;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ColourRandomizerTest {

	ColourRandomizer randomizer;
	
	@Before
	public void setUp() throws Exception {
		randomizer = new ColourRandomizer();
		randomizer.createPoolOfColours(Level.LEVEL3);
	}

	@After
	public void tearDown() throws Exception {
		randomizer = null;
	}
	
	@Test
	public void createPoolOfColoursCreatesCorrectAmountOfColours() {
		assertSame(4, randomizer.getPoolOfColours().size());
	}
	
	@Test
	public void poolOfColoursHasOnlyDifferentColours() {
		for(int i = 0; i < 3; i++) {
			for (int j = i+1; j < 4; j++) {
				assertNotSame(randomizer.getPoolOfColours().get(i),
						randomizer.getPoolOfColours().get(j));
			}
		}
	}
	
	@Test
	public void noneOfTheColoursIsTransparent() {
		for (Colour color : randomizer.getPoolOfColours()) {
			assertNotSame(Colour.TRANSPARENT, color);
		}
	}
}
