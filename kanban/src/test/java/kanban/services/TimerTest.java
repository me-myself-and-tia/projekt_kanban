package kanban.services;

import kanban.model.CurrentGame;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class TimerTest {

	private CurrentGame game;
	private Timer timer;
	
	@Before
	public void setUp() throws Exception {
		game = new CurrentGame();
		timer = new Timer(game);
	}

	@After
	public void tearDown() throws Exception {
		game = null;
		timer = null;
	}
	
	@Test
	public void timeIncrementedByOne() {
		game.setCurrentTime(20);
		timer.incrementTime();
		assertSame(21, game.getCurrentTime());
	}
	
	@Test
	public void secondsSmallerThanTenConvertedCorrectly() {
		game.setCurrentTime(1);
		assertEquals("00:01", timer.timeToMinutes());
	}
	
	@Test
	public void secondsSmallerThanSixtyConvertedCorrectly() {
		game.setCurrentTime(30);
		assertEquals("00:30", timer.timeToMinutes());
	}
	
	@Test
	public void secondsBiggerThanSixtyConvertedCorrectly() {
		game.setCurrentTime(70);
		assertEquals("01:10", timer.timeToMinutes());
	}
	
	@Test
	public void minutesBiggerThanTenConvertedCorrectly() {
		game.setCurrentTime(3540);
		assertEquals("59:00", timer.timeToMinutes());
	}
}
