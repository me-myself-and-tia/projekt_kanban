package kanban.services;

import kanban.model.CurrentGame;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GameStatusTest {

	GameStatus status;
	CurrentGame game;
	
	@Before
	public void setUp() throws Exception {
		game = new CurrentGame();
		status = new GameStatus(game);
	}

	@After
	public void tearDown() throws Exception {
		status = null;
	}
	
	@Test
	public void numberOfBlocksCorrectlyUpdated() {
		game.setCurrentNumberOfBlocks(100);
		status.updateNumberOfBlocks(10);
		assertEquals(90, game.getCurrentNumberOfBlocks());
	}
	
	@Test
	public void numberOfPointsCorrectlyUpdated() {
		game.setCurrentPoints(100);
		status.updatePoints(10);
		assertEquals(150, game.getCurrentPoints());
	}

}
