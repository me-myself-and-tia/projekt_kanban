package kanban.services;

import kanban.model.Block;
import kanban.model.Colour;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class BlockDeleterTest {

	private Block block1;
	private Block block2;
	private Block block3;
	private Block block4;
	private BlockDeleter deleter;
	private int numberOfDeletions;
	
	@Before
	public void setUp() throws Exception {
		block1 = new Block(Colour.TRANSPARENT);
		block2 = new Block(Colour.LIME);
		block3 = new Block(Colour.LIME);
		block4 = new Block(Colour.AQUA);
		block2.getNeighbours().set(Block.LEFT, block1);
		block2.getNeighbours().set(Block.DOWN, block4);
		block2.getNeighbours().set(Block.RIGHT, block3);
		numberOfDeletions = 0;
	}

	@After
	public void tearDown() throws Exception {
		deleter = null;
	}
	
	@Test
	public void deletesCorrectNeighbourBlocksWithoutRecursion() {
		deleter = new BlockDeleter(block2);
		numberOfDeletions =  deleter.deleteBlocksWithSameColour(block2);
		
		assertSame(Colour.TRANSPARENT, block2.getColour());
		assertNull(block2.getNeighbours().get(Block.UP));
		assertSame(Colour.TRANSPARENT, block1.getColour());
		assertSame(Colour.TRANSPARENT, block3.getColour());
		assertSame(Colour.AQUA, block4.getColour());
		assertSame(2, numberOfDeletions);
	}
	
	@Test
	public void deletesCorrectNeighbourBlocksWithRecursion() {
		Block block5 = new Block(Colour.LIME);
		Block block6 = new Block(Colour.LIME);
		block3.getNeighbours().set(Block.DOWN, block5);
		block5.getNeighbours().set(Block.DOWN, block6);
		
		deleter = new BlockDeleter(block2);
		numberOfDeletions =  deleter.deleteBlocksWithSameColour(block2);
		
		assertSame(Colour.TRANSPARENT, block2.getColour());
		assertNull(block2.getNeighbours().get(Block.UP));
		assertSame(Colour.TRANSPARENT, block1.getColour());
		assertSame(Colour.TRANSPARENT, block3.getColour());
		assertSame(Colour.AQUA, block4.getColour());
		assertSame(Colour.TRANSPARENT, block5.getColour());
		assertSame(Colour.TRANSPARENT, block6.getColour());
		assertSame(4, numberOfDeletions);
	}

}
