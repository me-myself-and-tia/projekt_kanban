package kanban.services;

import static org.junit.Assert.*;
import kanban.model.Block;
import kanban.model.Board;
import kanban.model.Colour;
import kanban.model.Level;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BoardSetterTest {

	BoardSetter setter;
	Board board;
	
	@Before
	public void setUp() throws Exception {
		setter = new BoardSetter();
		board = setter.initBoard(Level.LEVEL2);
	}

	@After
	public void tearDown() throws Exception {
		setter = null;
		board = null;
	}
	
	@Test
	public void initSetsColourRandomizer() {
		assertNotNull(setter.getColRandomizer());
		assertSame(3, setter.getColRandomizer().getPoolOfColours().size());
	}
	
	@Test
	public void initSetsBoardWithCorrectNumberOfElements() {
		assertNotNull(board);
		assertEquals(800, board.getBlockList().size());
	}

	@Test
	public void neighboursAreSetForRandomBlock() {
		assertNotNull(board.getBlockList().get(345).getNeighbours().get(Block.UP));
		assertNotNull(board.getBlockList().get(345).getNeighbours().get(Block.RIGHT));
		assertNotNull(board.getBlockList().get(345).getNeighbours().get(Block.DOWN));
		assertNotNull(board.getBlockList().get(345).getNeighbours().get(Block.LEFT));
	}
	
	@Test
	public void testIfClickableBlockExistsReturnsFalseIfNoneExists() {
		for (Block block : board.getBlockList()) {
			block.setColour(Colour.TRANSPARENT);
		}
		board.getBlockList().get(700).setColour(Colour.AQUA);
		assertFalse(setter.clickableBlockExists());
	}
	
	@Test
	public void testIfClickableBlockExistsReturnsTrue() {
		board.getBlockList().get(700).setColour(Colour.FUCHSIA);
		board.getBlockList().get(701).setColour(Colour.FUCHSIA);
		assertTrue(setter.clickableBlockExists());
	}
}
