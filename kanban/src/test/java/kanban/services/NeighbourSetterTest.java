package kanban.services;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import kanban.model.Block;
import kanban.model.Board;
import kanban.model.Colour;
import kanban.services.NeighbourSetter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NeighbourSetterTest {

	private NeighbourSetter setter;
	private List<Block> allBlocks = new ArrayList<Block>();

	@Before
	public void setUp() throws Exception {
		while(allBlocks.size() < Board.HEIGHT*Board.WIDTH) {
			allBlocks.add(new Block(Colour.YELLOW));
		}
		setter = new NeighbourSetter(allBlocks);
	}

	@After
	public void tearDown() throws Exception {
		setter = null;
	}

	@Test
	public void testSetNeighboursForUpperLeftCorner() {
		Block upperLeftCorner = allBlocks.get(0);
		setter.updateNeighbours(upperLeftCorner);
		
		assertNull(upperLeftCorner.getNeighbours().get(Block.UP));
		assertSame(upperLeftCorner.getNeighbours().get(Block.RIGHT), allBlocks.get(1));
		assertSame(upperLeftCorner.getNeighbours().get(Block.DOWN), allBlocks.get(40));
		assertNull(upperLeftCorner.getNeighbours().get(Block.LEFT));
	}
	
	@Test
	public void testSetNeighboursForUpperRightCorner() {
		Block upperRightCorner = allBlocks.get(39);
		setter.updateNeighbours(upperRightCorner);
		
		assertNull(upperRightCorner.getNeighbours().get(Block.UP));
		assertNull(upperRightCorner.getNeighbours().get(Block.RIGHT));
		assertSame(upperRightCorner.getNeighbours().get(Block.DOWN), allBlocks.get(79));
		assertSame(upperRightCorner.getNeighbours().get(Block.LEFT),allBlocks.get(38));
	}
	
	@Test
	public void testSetNeighboursForLowerLeftCorner() {
		Block lowerLeftCorner = allBlocks.get(760);
		setter.updateNeighbours(lowerLeftCorner);
		
		assertSame(lowerLeftCorner.getNeighbours().get(Block.UP), allBlocks.get(720));
		assertSame(lowerLeftCorner.getNeighbours().get(Block.RIGHT), allBlocks.get(761));
		assertNull(lowerLeftCorner.getNeighbours().get(Block.DOWN));
		assertNull(lowerLeftCorner.getNeighbours().get(Block.LEFT));
	}
	
	@Test
	public void testSetNeighboursForLowerRightCorner() {
		Block lowerRightCorner = allBlocks.get(799);
		setter.updateNeighbours(lowerRightCorner);
		
		assertSame(lowerRightCorner.getNeighbours().get(Block.UP), allBlocks.get(759));
		assertNull(lowerRightCorner.getNeighbours().get(Block.RIGHT));
		assertNull(lowerRightCorner.getNeighbours().get(Block.DOWN));
		assertSame(lowerRightCorner.getNeighbours().get(Block.LEFT),allBlocks.get(798));
	}
	
	@Test
	public void testSetNeighboursForBlockAwayFromEdge() {
		Block middle = allBlocks.get(41);
		setter.updateNeighbours(middle);
		
		assertSame(middle.getNeighbours().get(Block.UP), allBlocks.get(1));
		assertSame(middle.getNeighbours().get(Block.RIGHT), allBlocks.get(42));
		assertSame(middle.getNeighbours().get(Block.DOWN), allBlocks.get(81));
		assertSame(middle.getNeighbours().get(Block.LEFT),allBlocks.get(40));
	}

}
