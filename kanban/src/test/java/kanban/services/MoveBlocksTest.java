package kanban.services;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import kanban.model.Block;
import kanban.model.Board;
import kanban.model.Colour;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MoveBlocksTest {

	private MoveBlocks mover;
	private NeighbourSetter setter;
	private List<Block> allBlocks = new ArrayList<Block>();
	int numberOfBlocks;
	
	@Before
	public void setUp() throws Exception {
		numberOfBlocks = Board.HEIGHT * Board.WIDTH;
		while(allBlocks.size() < 800) {
			allBlocks.add(new Block(Colour.YELLOW));
		}
	}

	@After
	public void tearDown() throws Exception {
		mover = null;
		setter = null;
		allBlocks = null;
	}
	
	@Test
	public void moveBlocksWorksForTransparentBlocksAtTheBottomOfTheBoard() {
		setSeveralBlocksAtTheBottomToTransparent();
		setNeighbours();
		moveBlocks();
		
		for(int i = 780; i < 785; i++) {
			assertSame(Colour.YELLOW, allBlocks.get(i).getColour());
		}
		assertSame(Colour.YELLOW, allBlocks.get(742).getColour());
		
		for(int j = 20; j < 25; j++) {
			assertSame(Colour.TRANSPARENT, allBlocks.get(j).getColour());
		}
		assertSame(Colour.TRANSPARENT, allBlocks.get(62).getColour());
	}
	
	@Test
	public void moveBlocksWorksForTransparentBlocksOnTheLeftSideOfTheBoard() {
		setSeveralBlocksOnTheLeftToTransparent();
		setNeighbours();
		moveBlocks();

		for(int i = 0; i < 161; i+=40) {
			assertSame(Colour.TRANSPARENT, allBlocks.get(i).getColour());
		}
				
		assertSame(Colour.TRANSPARENT, allBlocks.get(1).getColour());
		assertSame(Colour.TRANSPARENT, allBlocks.get(41).getColour());
		assertSame(Colour.TRANSPARENT, allBlocks.get(2).getColour());
		
		assertSame(Colour.YELLOW, allBlocks.get(201).getColour());
		assertSame(Colour.YELLOW, allBlocks.get(202).getColour());
		assertSame(Colour.YELLOW, allBlocks.get(241).getColour());
		
		
		for(int i = 120; i < 320; i+=40) {
			allBlocks.set(i, new Block(Colour.TRANSPARENT));
		}
		allBlocks.set(201, new Block(Colour.TRANSPARENT));
		allBlocks.set(202, new Block(Colour.TRANSPARENT));
		allBlocks.set(241, new Block(Colour.TRANSPARENT));
	}
	
	@Test
	public void moveColumnsWorksForOneEmptyColumnInTheMiddleOfTheBoard() {
		setEmptyColumnInTheMiddleOfTheBoard();
		setTransparentBlocksOnTheRightOfEmptyColumn();
		setNeighbours();
		moveColumns();
		
		for(int i = 120; i < 800; i+=40) {
			assertSame(Colour.YELLOW, allBlocks.get(i).getColour());
		}
		
		assertSame(Colour.TRANSPARENT, allBlocks.get(20).getColour());
		assertSame(Colour.TRANSPARENT, allBlocks.get(60).getColour());
		
		for(int j = 39; j < 80; j+=40) {
			assertSame(Colour.TRANSPARENT, allBlocks.get(j).getColour());
		}
	}
	
	@Test
	public void moveColumnsWorksForTwoEmptyColumnsInTheMiddleOfTheBoard() {
		setEmptyColumnInTheMiddleOfTheBoard();
		setSecondEmptyColumnInTheMiddleOfTheBoard();
		setBlocksToLeftOfEmptyColumn();
		setNeighbours();
		moveColumns();
		
		//first former empty column
		for(int i = 20; i < 800; i+=40) {
			assertSame(Colour.YELLOW, allBlocks.get(i).getColour());
		}
		
		//second former empty column
		for(int i = 21; i < 800; i+=40) {
			assertSame(Colour.YELLOW, allBlocks.get(i).getColour());
		}
		
		//last column
		for(int j = 39; j < 800; j+=40) {
			assertSame(Colour.TRANSPARENT, allBlocks.get(j).getColour());
		}
		//second to last column
		for(int j = 38; j < 800; j+=40) {
			assertSame(Colour.TRANSPARENT, allBlocks.get(j).getColour());
		}
	}

	
	// helper methods
	
	private void setNeighbours() {
		setter = new NeighbourSetter(allBlocks);
		for (Block block : allBlocks) {
			setter.updateNeighbours(block);
		}
	}
	
	private void moveBlocks() {
		mover = new MoveBlocks(allBlocks);
		mover.moveBlocks();
	}
	
	private void moveColumns() {
		mover = new MoveBlocks(allBlocks);
		mover.moveColums();
	}
	
	private void setSeveralBlocksAtTheBottomToTransparent() {
		for(int i = 780; i < 785; i++) {
			allBlocks.set(i, new Block(Colour.TRANSPARENT));
		}
		allBlocks.set(742, new Block(Colour.TRANSPARENT));
	}
	
	private void setSeveralBlocksOnTheLeftToTransparent() {
		// 5 blocks on the left
		for(int i = 120; i < 281; i+=40) {
			allBlocks.set(i, new Block(Colour.TRANSPARENT));
		}
		allBlocks.set(201, new Block(Colour.TRANSPARENT));
		allBlocks.set(202, new Block(Colour.TRANSPARENT));
		allBlocks.set(241, new Block(Colour.TRANSPARENT));
	}
	
	private void setEmptyColumnInTheMiddleOfTheBoard() {
		for(int i = 20; i < 800; i+=40) {
			allBlocks.set(i, new Block(Colour.TRANSPARENT));
		}
	}
	
	private void setSecondEmptyColumnInTheMiddleOfTheBoard() {
		for(int i = 21; i < 800; i+=40) {
			allBlocks.set(i, new Block(Colour.TRANSPARENT));
		}
	}
	
	private void setTransparentBlocksOnTheRightOfEmptyColumn() {
		allBlocks.set(21, new Block(Colour.TRANSPARENT));
		allBlocks.set(61, new Block(Colour.TRANSPARENT));
	}
	
	private void setBlocksToLeftOfEmptyColumn() {
		allBlocks.set(19, new Block(Colour.TRANSPARENT));
		allBlocks.set(59, new Block(Colour.TRANSPARENT));
	}
}
