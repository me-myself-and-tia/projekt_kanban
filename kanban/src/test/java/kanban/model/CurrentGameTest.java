package kanban.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CurrentGameTest {

	CurrentGame game;
	
	@Before
	public void setUp() throws Exception {
		game = new CurrentGame();
	}

	@After
	public void tearDown() throws Exception {
		game = null;
	}
	
	@Test
	public void currentGameConstructorSetsCorrectValues() {
		assertSame(Level.LEVEL1, game.getCurrentLevel());
		assertSame(0, game.getTotalPoints());
	}
	
	@Test
	public void isWonReturnsTrueForClearBoard() {
		game.setCurrentNumberOfBlocks(0);
		assertTrue(game.isWon());
	}
	
	@Test
	public void isWonReturnsTrueForWonGame() {
		game.setCurrentNumberOfBlocks(4);
		assertTrue(game.isWon());
	}
	
	@Test
	public void isWonReturnsFalseForCompleteBoard() {
		game.setCurrentNumberOfBlocks(1250);
		assertFalse(game.isWon());
	}
	
	@Test
	public void testSetPoints() {
		game.setCurrentPoints(200);
		assertEquals(200, game.getCurrentPoints());
	}
	
	@Test
	public void testSetTime() {
		game.setCurrentTime(70);
		assertEquals(70, game.getCurrentTime());
	}
	
	@Test
	public void testSetCurrentBlocs() {
		game.setCurrentNumberOfBlocks(1000);
		assertEquals(1000, game.getCurrentNumberOfBlocks());
	}
	public void testSetCurrentNumberOfBlocks() {
		game.setCurrentNumberOfBlocks(50);
		assertEquals(50, game.getCurrentNumberOfBlocks());
	}
	
	public void testSetCurrentLevel() {
		game.setCurrentLevel(Level.LEVEL4);
		assertEquals(Level.LEVEL4, game.getCurrentLevel());
	}
	
	public void testSetBoard() {
		List<Block>liste = new ArrayList<Block>();
		Board board = new Board(liste);
		game.setBoard(board);
		assertEquals(board, game.getBoard());
	}
}
