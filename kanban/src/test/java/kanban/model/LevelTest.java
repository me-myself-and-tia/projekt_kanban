package kanban.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class LevelTest {

	@Test
	public void testGetNumberOfColours() {
		assertSame(4, Level.LEVEL3.getNumberOfColours());
	}
	
	@Test
	public void testGetGoal() {
		assertSame(20, Level.LEVEL2.getGoal());
	}
	
	@Test
	public void testGetLevelNumber() {
		assertEquals("4", Level.LEVEL4.getLevelNumber());
	}
	
	@Test
	public void testgetLevelNumberAsInt() {
		assertEquals(2, Level.LEVEL2.getLevelNumberAsInt());
	}
	
	@Test
	public void threeIsNotLastLevel() {
		assertFalse(Level.LEVEL3.isLastlevel());
	}
	
	@Test
	public void fourIsNotLastLevel() {
		assertTrue(Level.LEVEL4.isLastlevel());
	}
}
