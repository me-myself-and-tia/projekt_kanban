package kanban.model;

import static org.junit.Assert.*;

import org.junit.Test;

public class ColourTest {

	
	@Test
	public void testToStringMethodReturnsLowerCaseColour() {
		assertEquals("aqua", Colour.AQUA.toString());
	}
	
	@Test
	public void getRandomColourReturnsColourObject() {
		Colour random = Colour.getRandomColour();
		assertEquals(Colour.class, random.getClass());
	}

}
