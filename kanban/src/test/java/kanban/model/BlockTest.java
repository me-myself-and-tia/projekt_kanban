package kanban.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import kanban.model.Block;
import kanban.model.Colour;
import kanban.services.NeighbourSetter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BlockTest {

	private Block block;

	@Before
	public void setUp() throws Exception {
		block = new Block(Colour.FUCHSIA);
	}

	@After
	public void tearDown() throws Exception {
		block = null;
	}
	
	@Test
	public void testGetColour() {
		assertSame(Colour.FUCHSIA, block.getColour());
	}
	
	@Test
	public void testSetColour() {
		block.setColour(Colour.GREEN);
		assertSame(Colour.GREEN, block.getColour());
	}
	
	@Test
	public void testGetNeighboursWithMiddleBlock() {
		generateNonClickableBlockInBlocklist();
		assertSame(Colour.AQUA, block.getNeighbours().get(Block.UP).getColour());
		assertSame(Colour.YELLOW, block.getNeighbours().get(Block.RIGHT).getColour());
		assertSame(Colour.ORANGE, block.getNeighbours().get(Block.DOWN).getColour());
		assertSame(Colour.AQUA, block.getNeighbours().get(Block.LEFT).getColour());
	}
	
	@Test
	public void testGetNeighboursWithBlockAtEdge() {
		generateClickableBlockInUpperLeftCorner();
		assertNull(block.getNeighbours().get(Block.UP));
		assertSame(Colour.FUCHSIA, block.getNeighbours().get(Block.RIGHT).getColour());
		assertSame(Colour.PURPLE, block.getNeighbours().get(Block.DOWN).getColour());
		assertNull(block.getNeighbours().get(Block.LEFT));
	}

	@Test
	public void testifClickableBlockIsClickable() {
		generateClickableBlockInUpperLeftCorner();
		assertTrue(block.isClickable());
	}
	
	@Test
	public void testifNotClickableBlockIsNotClickable() {
		generateNonClickableBlockInBlocklist();
		assertFalse(block.isClickable());
	}
	
	
	@Test
	public void testIfTransparentBlockReturnsTrue() {
		Block transparent = new Block(Colour.TRANSPARENT);
		assertTrue(transparent.isTransparent());
	}
	
	@Test
	public void testIfNonTransparentBlockReturnsFalse() {
		assertFalse(block.isTransparent());
	}
	
	
	// helper methods
	
	private void generateNonClickableBlockInBlocklist() {
		List<Block> blockList = new ArrayList<Block>();
		for(int i = 0; i < 624; i++) {
			blockList.add(new Block(Colour.AQUA));
		}
		blockList.add(block);
		blockList.add(new Block(Colour.YELLOW));
		for(int i = 0; i < 624; i++) {
			blockList.add(new Block(Colour.ORANGE));
		}
		NeighbourSetter setter = new NeighbourSetter(blockList);
		setter.updateNeighbours(block);
	}
	
	private void generateClickableBlockInUpperLeftCorner() {
		List<Block> blockList = new ArrayList<Block>();
		blockList.add(block);
		blockList.add(new Block(Colour.FUCHSIA));
		for(int i = 0; i < 1248; i++) {
			blockList.add(new Block(Colour.PURPLE));
		}
		NeighbourSetter setter = new NeighbourSetter(blockList);
		setter.updateNeighbours(block);
	}

}
