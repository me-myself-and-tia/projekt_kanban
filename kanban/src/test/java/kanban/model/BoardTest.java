package kanban.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BoardTest {

	private Board board;
	private List<Block> blockList;
	
	@Before
	public void setUp() throws Exception {
		generateBlocklist();
		board = new Board(blockList);
	}

	@After
	public void tearDown() throws Exception {
		board = null;
	}
	
	@Test
	public void testGetWidth() {
		assertSame(40, board.getWidth());
	}
	
	@Test
	public void testGetHeight() {
		assertSame(20, board.getHeight());
	}
	
	@Test
	public void testGetBlockList() {
		assertSame(blockList, board.getBlockList());
	}

	
	// Helper methods
	
	private void generateBlocklist() {
		blockList = new ArrayList<Block>();
		for(int i = 0; i < 624; i++) {
			blockList.add(new Block(Colour.AQUA));
		}
		blockList.add(new Block(Colour.FUCHSIA));
		blockList.add(new Block(Colour.YELLOW));
		for(int i = 0; i < 624; i++) {
			blockList.add(new Block(Colour.ORANGE));
		}
	}
}
